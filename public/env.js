window.env = {
  ENV: 'development',
  API_ENDPOINT: 'https://api.misakey.com.local',
  AUTH_ENDPOINT: 'https://api.misakey.com.local/misadmin/auth',
};
