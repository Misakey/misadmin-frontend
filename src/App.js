import React from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import JssProvider from 'react-jss/lib/JssProvider';

import Admin from './components/Admin';
import CallbackRoute from './components/CallbackRoute';


import generateClassName from './tools/generateClassName';

export const history = createBrowserHistory();

const App = () => (
  <Router history={history}>
    <>
      <CallbackRoute />

      <JssProvider generateClassName={generateClassName}>
        <Admin history={history} />
      </JssProvider>
    </>
  </Router>
);

export default App;
