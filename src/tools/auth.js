import { parseJwt } from './jwt';


export const processAuth = location => new Promise((resolve, reject) => {
  const { hash } = location;

  const queryParameters = hash.indexOf('#') === 0 ? hash.replace('#', '?') : hash;

  if (queryParameters) {
    const params = new URLSearchParams(queryParameters);

    const accessToken = params.get('access_token');

    const idToken = params.get('id_token');

    const parsedIdToken = parseJwt(idToken);

    const userId = parsedIdToken.sub;

    if (accessToken && idToken && userId) {
      const init = {
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${accessToken}`,
        },
      };

      fetch(`${window.env.API_ENDPOINT}/users/${userId}`, init)
        .then(((rawResponse) => {
          if (rawResponse.status === 200) {
            const contentType = rawResponse.headers.get('Content-Type') || '';
            if (contentType.startsWith('application/json')) {
              rawResponse.json().then((response) => {
                const { email } = response;
                if (email.endsWith('@misakey.com') && email.replace(/[^@]/g, '').length === 1) {
                  localStorage.setItem('access_token', accessToken);
                  localStorage.setItem('id_token', idToken);
                  resolve();
                } else {
                  const error = new Error('not a misadmin');
                  reject(error);
                }
              });
            } else {
              reject(new Error('Nojson error'));
            }
          } else {
            reject(new Error('Unhandled response'));
          }
        }))
        .catch(reject);
    } else {
      let searchParams = new URLSearchParams(location.search);

      if (params.get('error')) {
        searchParams = params;
      }

      if (searchParams.get('error')) {
        const error = new Error(searchParams.get('error'));
        error.description = searchParams.get('error_description');
        error.hint = searchParams.get('error_hint');
        reject(error);
      } else {
        reject(new Error('unknown error'));
      }
    }
  } else {
    reject(new Error('No query params'));
  }
});
