// Tools for manipulating JSON Web Tokens (JWT)
// see https://jwt.io/

const JWT_PATTERN = new RegExp('^[A-Za-z0-9+/=]+\\.[A-Za-z0-9+/=]+\\..+$');

export function parseJwt(data) {
  // eslint-disable-next-line no-console
  console.assert(JWT_PATTERN.test(data), 'given data does not match JWT pattern');
  const payload = data.split('.')[1];
  return JSON.parse(atob(payload));
}
