import { AUTH_LOGIN, AUTH_LOGOUT, AUTH_ERROR, AUTH_CHECK } from 'react-admin';

import { generateRandomState } from '../tools/misc';

export default (type, params) => {
  // called when the user attempts to log in
  if (type === AUTH_LOGIN) {
    const accessToken = localStorage.getItem('access_token');
    if (!accessToken) {
      // Require to login, so redirect the user to auth page
      const baseUrl = window.env.AUTH_ENDPOINT;
      const state = generateRandomState();

      window.location.assign(`${baseUrl}?state=${state}`);
      return Promise.reject();
    }
    // accept all username/password combinations
    return Promise.resolve();
  }
  // called when the user clicks on the logout button
  if (type === AUTH_LOGOUT) {
    localStorage.removeItem('access_token');
    localStorage.removeItem('id_token');
    return Promise.resolve();
  }
  // called when the API returns an error
  if (type === AUTH_ERROR) {
    const { status } = params;
    if (status === 401 || status === 403) {
      localStorage.removeItem('access_token');
      localStorage.removeItem('id_token');
      return Promise.reject();
    }
    return Promise.resolve();
  }
  // called when the user navigates to a new location
  if (type === AUTH_CHECK) {
    return localStorage.getItem('access_token')
      ? Promise.resolve()
      : Promise.reject();
  }
  return Promise.reject(new Error('Unknown method'));
};
