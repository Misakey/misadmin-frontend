import {
  GET_LIST,
  GET_ONE,
  GET_MANY_REFERENCE,
  CREATE,
  UPDATE,
  DELETE,
  fetchUtils,
} from 'react-admin';
import { stringify } from 'query-string';

const API_URL = window.env.API_ENDPOINT;

/**
 * @param {String} type One of the constants appearing at the top of this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} { url, options } The HTTP request parameters
 */
const convertDataProviderRequestToHTTP = (type, resource, params) => {
  const headers = new Headers({ Accept: 'application/json' });

  const token = localStorage.getItem('access_token');
  headers.set('Authorization', `Bearer ${token}`);


  const url = `${API_URL}/${resource}/${params.id}`;

  switch (type) {
    case GET_LIST: {
      if (resource === 'applications') {
        const extraSearch = (params.filter.search && params.filter.search.length >= 1) ? `/${params.filter.search}` : '/';
        return { url: `${API_URL}/${resource}${extraSearch}`, options: { headers } };
      }
      const query = {
        ...params.filter,
        search: (params.filter.search && params.filter.search.length >= 1)
          ? params.filter.search : undefined,
        limit: params.pagination.perPage,
        offset: (params.pagination.page - 1) * params.pagination.perPage,
      };
      return { url: `${API_URL}/${resource}?${stringify(query)}`, options: { headers } };
    }
    case GET_ONE: {
      const baseObject = { url, options: { headers } };
      if (resource === 'application-info') {
        return {
          ...baseObject,
          customType: 'getSingleApplications',
        };
      }
      return baseObject;
    }

    case UPDATE: {
      if (resource === 'application-info') {
        const { domains, links, new_logo: newLogo, ...rest } = params.data;
        const patchBody = {
          domains: domains.map(domain => ({ uri: domain.uri })),
          links: links.map(link => ({
            type: link.type,
            value: link.value,
          })),
          application_info: rest,
        };

        return {
          url: `${API_URL}/applications/${params.id}`,
          options: { method: 'PUT', body: JSON.stringify(patchBody), headers },
          customType: (newLogo !== undefined) ? 'custom-application-info-update-with-image' : undefined,
        };
      }
      throw new Error(`Unsupported fetch action type ${type}`);
    }

    case DELETE:
      return { url, options: { method: 'DELETE', headers } };

    default:
      throw new Error(`Unsupported fetch action type ${type}`);
  }
};

/**
 * @param {Object} response HTTP response from fetch()
 * @param {String} type One of the constants appearing at the top of this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} Data Provider response
 */
const convertHTTPResponseToDataProvider = async (response, type, resource, params) => {
  const { json } = response;
  switch (type) {
    case GET_LIST:
      if (resource === 'application-contributions') {
        const applicationIds = [...new Set(json.map(contribution => contribution.application_id))];

        const appsInfoResponse = await fetch(`${API_URL}/application-info?ids=${applicationIds.join(',')}`);
        const appsInfo = await appsInfoResponse.json();

        return {
          data: json.map(contribution => ({
            ...contribution,
            main_domain: appsInfo.find(app => app.id === contribution.application_id).main_domain,
          })),
          total: (json.length === params.pagination.perPage) ? 1000 : json.length,
        };
      }
      return {
        data: json,
        total: (json.length === params.pagination.perPage) ? 1000 : json.length,
      };

    case GET_MANY_REFERENCE:
      return {
        data: json,
        total: 1000,
      };
    case CREATE:
      return { data: { ...params.data, id: json.id } };
    case UPDATE:
      return { data: { ...params.previousData, ...params.data } };
    case DELETE:
      return { data: { id: params.previousData.id } };
    default:
      return { data: json };
  }
};

/**
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the request type
 * @returns {Promise} the Promise for response
 */
export default (type, resource, params) => {
  const { fetchJson } = fetchUtils;
  const convertedRequest = convertDataProviderRequestToHTTP(type, resource, params);
  const { url, options } = convertedRequest;
  const { customType } = convertedRequest;
  if (customType === 'getSingleApplications') {
    // Get application-info to get
    return fetchJson(url, options)
      .then((applicationInfoResponse) => {
        // Get applications to get links & domains
        const mainDomain = applicationInfoResponse.json.main_domain;
        const applicationsUrl = `${API_URL}/applications/${mainDomain}`;
        return fetchJson(applicationsUrl, options)
          .then((applicationsResponse) => {
            const globalResponse = applicationsResponse;
            globalResponse.json = {
              ...applicationInfoResponse.json,
              domains: applicationsResponse.json.domains,
              links: applicationsResponse.json.links,
            };
            if (globalResponse.json.weight < 100) {
              globalResponse.json.weight = 100;
            }
            return convertHTTPResponseToDataProvider(globalResponse, type, resource, params);
          });
      });
  }
  if (customType === 'custom-application-info-update-with-image') {
    const imageHeaders = options.headers;
    return fetchJson(url, options)
      .then((response) => {
        // Proceed update image
        const updateImageUrl = `${API_URL}/application-info/${params.id}/logo`; // PUT with `logo` as FormData
        const imageData = new FormData();
        imageData.append('logo', params.data.new_logo.rawFile);

        imageHeaders.delete('content-type');
        return fetch(updateImageUrl, {
          method: 'PUT',
          headers: imageHeaders,
          body: imageData,
        }).then((imageResponse) => {
          if (imageResponse.status === 204) {
            return convertHTTPResponseToDataProvider(response, type, resource, params);
          }
          throw new Error(`Error ${imageResponse.status} with image update`);
        });
      });
  }
  return fetchJson(url, options)
    .then(response => convertHTTPResponseToDataProvider(response, type, resource, params));
};
