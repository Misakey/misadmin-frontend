import React from 'react';

import { Route } from 'react-router-dom';

import { processAuth } from '../tools/auth';

const CallbackRoute = () => (
  <Route
    path="/callback"
    render={(renderProps) => {
      processAuth(renderProps.location)
        .then(() => {
          renderProps.history.replace('/application-info');
        })
        .catch(console.error);
      return null;
    }}
    noLayout
  />
);

export default CallbackRoute;
