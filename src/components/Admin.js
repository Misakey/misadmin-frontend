import React from 'react';
import PropTypes from 'prop-types';

import { Admin, Resource } from 'react-admin';

import ApplicationInfoEdit from './ApplicationInfo/Edit';
import ApplicationInfoList from './ApplicationInfo/List';

import ApplicationContributionsList from './ApplicationContributions/List';

import authProvider from '../Provider/auth';
import dataProvider from '../Provider/data';

import LoginPage from './LoginPage';

function MisAdmin({ history }) {
  return (
    <Admin
      dataProvider={dataProvider}
      authProvider={authProvider}
      loginPage={LoginPage}
      history={history}
    >
      <Resource
        name="application-info"
        list={ApplicationInfoList}
        edit={ApplicationInfoEdit}
      />
      <Resource
        name="application-contributions"
        list={ApplicationContributionsList}
      />
    </Admin>
  );
}

MisAdmin.propTypes = {
  history: PropTypes.object.isRequired,
};

export default MisAdmin;
