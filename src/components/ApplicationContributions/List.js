import React from 'react';
import PropTypes from 'prop-types';
import { List, Datagrid, TextField, EmailField, DeleteButton } from 'react-admin';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  deleteButton: {
    width: '50px',
  },
};

const MyDeleteButton = ({ headerClassName, ...props }) => (
  <DeleteButton {...props} />
);

MyDeleteButton.propTypes = {
  headerClassName: PropTypes.string.isRequired,
};

const ApplicationContributionsList = ({ classes, ...props }) => (
  <List
    {...props}
    perPage={50}
    bulkActionButtons={false}
  >
    <Datagrid rowClick="edit">
      <TextField source="main_domain" sortable={false} />
      <TextField source="link" sortable={false} />
      <EmailField source="dpo_email" sortable={false} />
      <MyDeleteButton headerClassName={classes.deleteButton} />
    </Datagrid>
  </List>
);

ApplicationContributionsList.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(styles)(ApplicationContributionsList);
