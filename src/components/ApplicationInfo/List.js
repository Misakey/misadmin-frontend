import React from 'react';
import PropTypes from 'prop-types';
import {
  List, Datagrid, Filter, TextInput, BooleanInput, NumberInput,
  TextField, EmailField, ImageField, NumberField,
  Pagination,
} from 'react-admin';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  image: {
    height: '30px',
    width: '30px',
    minHeight: 'auto',
    minWidth: 'auto',
  },
};

const ApplicationInfoFilter = props => (
  <Filter {...props}>
    <TextInput label="Search" source="search" alwaysOn />
    <BooleanInput source="has_dpo_email" />
    <BooleanInput source="has_logo_uri" />
    <NumberInput source="lighter_than" />
  </Filter>
);

const ApplicationInfoPagination = props => (
  <Pagination rowsPerPageOptions={[25, 50, 100]} {...props} />
);

const ApplicationInfoList = ({ classes, ...props }) => (
  <List
    filters={<ApplicationInfoFilter />}
    {...props}
    perPage={50}
    pagination={<ApplicationInfoPagination />}
    bulkActionButtons={false}
  >
    <Datagrid rowClick="edit">
      <ImageField source="logo_uri" classes={classes} sortable={false} />
      <TextField source="name" sortable={false} />
      <TextField source="main_domain" sortable={false} />
      <EmailField source="dpo_email" sortable={false} />
      <NumberField source="weight" sortable={false} />
    </Datagrid>
  </List>
);

ApplicationInfoList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ApplicationInfoList);
