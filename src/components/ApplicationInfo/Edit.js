import React from 'react';
import {
  Edit, SimpleForm, TextInput, LongTextInput, NumberInput,
  ImageInput, ImageField, SelectInput, SaveButton, Toolbar,
  ArrayInput, SimpleFormIterator,
} from 'react-admin';

import { withStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';

import serviceTypeChoices from './enums/serviceType';
import privacyJuridictionChoices from './enums/privacyJuridiction';
import mainIncomeChoices from './enums/mainIncome';
import homeCountryChoices from './enums/homeCountry';
import linksTypeChoices from './enums/linksType';

const styles = {
  image: {
    height: '80px',
    width: '80px',
    minHeight: 'auto',
    minWidth: 'auto',
  },
};

const PostEditToolbar = props => (
  <Toolbar {...props}>
    <SaveButton submitOnEnter={false} />
  </Toolbar>
);

const MyDivider = () => (<Divider />);

// eslint-disable-next-line
const ApplicationInfoEdit = ({ classes, ...props }) => (
  <Edit {...props}>
    <SimpleForm toolbar={<PostEditToolbar />}>
      <ImageField source="logo_uri" classes={classes} label="Current Logo" />
      <ImageInput source="new_logo" label="New Logo" accept="image/*">
        <ImageField source="new_logo" />
      </ImageInput>
      <TextInput source="name" />
      <TextInput source="short_desc" />
      <LongTextInput source="long_desc" />
      <TextInput source="dpo_email" />

      <MyDivider />
      <ArrayInput source="links">
        <SimpleFormIterator>
          <SelectInput source="type" choices={linksTypeChoices} />
          <TextInput source="value" />
        </SimpleFormIterator>
      </ArrayInput>

      <MyDivider />

      <NumberInput source="weight" />

      <SelectInput source="service_type" choices={serviceTypeChoices} />
      <SelectInput source="privacy_juridiction" choices={privacyJuridictionChoices} />
      <SelectInput source="main_income" choices={mainIncomeChoices} />
      <SelectInput source="home_country" choices={homeCountryChoices} />
    </SimpleForm>
  </Edit>
);

export default withStyles(styles)(ApplicationInfoEdit);
