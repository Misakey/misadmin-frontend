Misadmin is an internal tool to edit the applications database. It helps us modify the dpo's emails, 
descriptions or app names that for now comes from the crawler.

## Misc

Misadmin is built on top of `react-admin`.

## Deployment

:warning: Make sure the corresponding images exist before running the following commands.

Create a `config.yaml` file with a `config:` key and the `env.js` production content (you can find an example in `/helm/config.sample.yaml`)

When you want to deploy the application for the first time, clone the repo, checkout to the desired tag, then run:

```
helm install --name misadmin-frontend helm/misadmin-frontend -f path/to/config.yaml --set env=production --set image.tag=`git describe --tags --always`

```

If you just need to upgrade the application, then run:
```
helm upgrade misadmin-frontend helm/misadmin-frontend -f path/to/config.yaml --set env=production --set image.tag=`git describe --tags --always`
```
